# README #

* SimpleChat is exactly what the name implies - a simple chat app. There are a list of active/inactive users, unapproved friends, friend requests, and of course, you can send messages to your friends. 
* You will have to have your own web server if you wish to make your own version of a simple chat application. 

### What is this repository for? ###

* This SimpleChat app was created based on a project on Udemy. I have made changes to it involving some additional functionality and UI improvements to make it look better. 
* This project is free for anyone to use a reference in their own work for any projects that they would like to use. 
* I hope that this simple app helps you in whatever goals you are trying to accomplish. Feel free to email me if you have any questions or would like help in any project that you are working with.
