package simplechat.thenotoriousrog.simplechat.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces.Socket;

public class Socketer implements Socket {

    // todo: we need to change this database url with a real database for my application that I have created.
    private static final String AUTHENTICATION_SERVER_ADDRESS = "http://127.0.0.1/androidchatter"; // this is the webserver address.

    private int listeningPort = 0;
    private static final String HTTP_REQUEST_FAILED = null;
    private HashMap<InetAddress, java.net.Socket> sockets = new HashMap<>();
    private ServerSocket serverSocket;
    private boolean listening;

    private class ReceiveConnection extends Thread {

        java.net.Socket clientSocket = null;
        public ReceiveConnection(java.net.Socket socket) {
            this.clientSocket = (java.net.Socket)socket;
            Socketer.this.sockets.put(socket.getInetAddress(), socket);
        }

        @Override
        public void run() {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
                String inputLine;

                while((inputLine = in.readLine()) != null) {
                    if(inputLine.equals("exit")) {
                        ((java.net.Socket)clientSocket).shutdownInput();
                        ((java.net.Socket)clientSocket).shutdownOutput();
                        ((java.net.Socket)clientSocket).close();
                       Socketer.this.sockets.remove(clientSocket.getInetAddress());
                    }
                }
            } catch (IOException ex) {
                System.out.println("IOException while reading data from sockets: ");
                ex.printStackTrace();
            }
        }

    }

    @Override
    public String sendHTTPRequest(String params) {

        URL url;
        String result = new String();
        try {
            url = new URL(AUTHENTICATION_SERVER_ADDRESS);
            HttpURLConnection connection;
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);

            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.println(params);
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            while((inputLine = in.readLine()) != null) {
                result = result.concat(inputLine);
            }
            in.close();

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        if(result.length() == 0) {
            result = HTTP_REQUEST_FAILED;
        }

        return result;
    }

    @Override
    public int startListeningPort(int portNum) {

        listening = true;
        try {
            serverSocket = new ServerSocket(portNum);
            this.listeningPort = portNum;
        } catch (IOException ex) {
            return 0;
        }

        while(listening) {
            try {
                new ReceiveConnection(serverSocket.accept()).start();
            } catch (IOException ex) {
                return 2;
            }
        }

        try {
            serverSocket.close();
        } catch (IOException ex) {
            System.out.println("Exception when trying to close server socket. ");
            ex.printStackTrace();
            return 3;
        }

        return 1;
    }

    @Override
    public void stopListening() { }

    @Override
    public void exit() {

       Iterator<java.net.Socket> iterator = sockets.values().iterator();
       while(iterator.hasNext()) {
           java.net.Socket socket =  iterator.next();

           try {
               socket.shutdownOutput();
               socket.shutdownOutput();
               socket.close();
           } catch (Exception ex) {
               ex.printStackTrace();
           }

       }

    }
}
