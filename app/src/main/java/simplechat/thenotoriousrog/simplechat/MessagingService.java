package simplechat.thenotoriousrog.simplechat;

import android.app.Service;
import android.net.ConnectivityManager;
import android.os.IBinder;

import java.util.Timer;

import simplechat.thenotoriousrog.simplechat.communication.Socketer;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces.Manager;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces.Updater;

public class MessagingService<InfoOfFriend> extends Service implements Manager, Updater {

    public static String USERNAME;
    public static final String TAKE_MESSAGE = "Take_Message";
    public static final String FRIEND_LIST_UPDATED = "Take Friend List";
    public static final String MESSAGE_LIST_UPDATED = "Take Message List";
    public ConnectivityManager conManager = null;
    private String rawFriendList = "";
    private String rawMessageList = "";

    Socketer socketOperator = new Socketer();
    private final IBinder binder = new IMBinder();
    private String username;
    private String password;
    private boolean authenticatedUser = false;
    private Timer timer;

}
