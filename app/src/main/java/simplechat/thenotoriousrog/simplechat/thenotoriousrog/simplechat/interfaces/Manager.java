package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces;

import java.io.UnsupportedEncodingException;

public interface Manager {

    public String getUsername(); // get the username from the device.
    public String sendMessage(String fromUser, String toUser, String message) throws UnsupportedEncodingException; // sends a message to a user.
    public boolean isNetworkConnected();
    public boolean isUserAuthenticated();
    public String getLastRawFriendList();
    public void exit();
    public String signUpUser(String usernameText, String passwordText, String email);
    public String addNewFriendRequest(String friendUsername);
    public String sendFriendsReqsResponse(String approvedFriendNames, String discardedFriendNames);
}
