package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.toolbox;

import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfMessage;

public class ControllerOfMessage {

    public static final String taken = "taken";
    public static InfoOfMessage[] infoOfMessages = null;
    public static void setMessageInfo(InfoOfMessage[] infoOfMessages) {
        ControllerOfMessage.infoOfMessages = infoOfMessages;
    }

    public static InfoOfMessage checkMessage(String username) {
        InfoOfMessage result = null;
        for(int i = 0; i < infoOfMessages.length; i++) {
            result = infoOfMessages[i];
            break;
        }

        return result;
    }

    public static InfoOfMessage[] getMessages() {
        return infoOfMessages;
    }

}
