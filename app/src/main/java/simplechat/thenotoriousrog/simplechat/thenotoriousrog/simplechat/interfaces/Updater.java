package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces;

import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfFriend;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfMessage;

public interface Updater {

    public void updateData(InfoOfMessage[] messages, InfoOfFriend[] friends, InfoOfFriend[] unapprovedFriends,
                           String userKey);


}
