package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof;

public enum InfoStatus {

    ONLINE, OFFLINE, UNAPPROVED, INVISIBLE, BUSY

}
