package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces;

/*
    * Used for updating the socket interface.
 */
public interface Socket {

    public String sendHTTPRequest(String params); // sends an http request to the user.
    public int startListeningPort(int portNum); // this will start listening on a part from the user.
    public void stopListening();
    public void exit();


}
