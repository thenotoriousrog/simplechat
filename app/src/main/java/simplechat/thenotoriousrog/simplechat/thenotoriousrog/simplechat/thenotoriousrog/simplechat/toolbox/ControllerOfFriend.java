package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.toolbox;



import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfFriend;

/*
    * Used to store information about the friends
 */
public class ControllerOfFriend {

    public static InfoOfFriend[] friendsInfo;
    public static InfoOfFriend[] unapprovedFriends;
    public static String activeFriends;

    public static void setFriendsInfo(InfoOfFriend[] friends) {
        ControllerOfFriend.friendsInfo = friends;
    }

    public static InfoOfFriend checkFriends(String username, String userKey) {
        InfoOfFriend result = null;
        if(friendsInfo != null) {
            for(int i = 0; i < friendsInfo.length; i++) {
                if(friendsInfo[i].USERNAME.equals(username) && friendsInfo[i].UserKey.equals(userKey)) {
                    result = friendsInfo[i];
                    break;
                }
            }
        }
        return result;
    }

    public static InfoOfFriend getFriendsInfo(String username) {
        InfoOfFriend result = null;
        if(friendsInfo != null) {
            for(int i = 0; i < friendsInfo.length; i++) {
                if(friendsInfo[i].USERNAME.equals(username)) {
                    result = friendsInfo[i];
                    break;
                }
            }
        }
        return result;
    }

}
