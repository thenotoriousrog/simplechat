package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof;

public class InfoOfFriend {

    public static final String FRIENDS_LIST = "friends_list";
    public static String USERNAME = "username";
    public static final String STATUS = "status";
    public static final String Port = "port";
    public static final String IP = "ip";
    public static final String UserKey = "userKey";
    public static final String Message = "message";

    public static String userName;
    public static InfoStatus status;
    public static String port;

}
