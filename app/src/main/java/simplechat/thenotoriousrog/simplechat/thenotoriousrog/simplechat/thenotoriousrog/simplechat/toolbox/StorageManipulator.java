package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.toolbox;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
    * Holds a series of commands that can be used to manipulate data into our database.
 */
public class StorageManipulator extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SimpleChat.db";
    public static final int DATABASE_VERSION = 1;
    public static final String _ID = "_id";
    public static final String table_name_message = "table_message";
    public static final String Message_Receiver = "receiver";
    public static final String Message_Sender = "sender";
    public static final String Message_MessageText = "message";

    public static final String TABLE_MESSAGE_CREATE =
            "CREATE TABLE " + table_name_message
                    + " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + Message_Receiver + " VARCHAR(25), "
                    + Message_Sender + " VARCHAR(25)";

    public static final String Table_Message_Drop = "DROP TABLE IF EXIST" + table_name_message;

    public StorageManipulator(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_MESSAGE_CREATE); // create the database tables.
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(Table_Message_Drop);
    }

    public void insert(String sender, String receiver, String message) {
        long rowId;
        try {
            SQLiteDatabase db = getWritableDatabase(); // opens a database to write to.
            ContentValues contentValues = new ContentValues();
            contentValues.put(Message_Receiver, receiver);
            contentValues.put(Message_Sender, sender);
            contentValues.put(Message_MessageText, message);

            rowId = db.insert(table_name_message, null, contentValues); // insert the items into our database.

        } catch (Exception ex) {
            System.out.println("Exception when trying to write to the database.");
            ex.printStackTrace();
        }
    }

    public Cursor get(String sender, String receiver) {
        SQLiteDatabase db = getWritableDatabase();
        String selectQuery = "SELECT * FROM" + table_name_message + "WHERE" + Message_Sender + "LIKE" + sender + "AND" + Message_Receiver
                + "LIKE" + receiver + "ORDER BY" + _ID + "ASC";


        return null;
    }
}
