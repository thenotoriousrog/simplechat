package simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.toolbox;

import android.icu.text.IDNA;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Vector;

import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces.Updater;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfFriend;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoOfMessage;
import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.typeof.InfoStatus;

public class ParserXML extends DefaultHandler {

    public String userKey = new String();
    public Updater updater;

    public ParserXML(Updater updater) {
        this.updater = updater;
    }

    private Vector<InfoOfFriend> friends = new Vector<>();
    private Vector<InfoOfFriend> onlineFriends = new Vector<>();
    private Vector<InfoOfFriend> unapprovedFriends = new Vector<>();
    private Vector<Updater> unreadMessages = new Vector<>();

    public void endDocument() throws SAXException {

        InfoOfFriend[] friendsList = new InfoOfFriend[friends.size()];
        Updater[] messages = new Updater[unreadMessages.size()];

        int onlineFriendsCount = onlineFriends.size();
        for(int i = 0; i < onlineFriendsCount; i++) {
            friendsList[i] = onlineFriends.get(i);
        }

        int offlineFriendsCount = friends.size();
        for(int i = 0; i < offlineFriendsCount; i++) {
            friendsList[i + onlineFriendsCount] = friends.get(i);
        }

        int unapprovedFriendsCount = unapprovedFriends.size();
        InfoOfFriend[] unapprovedFriendsList = new InfoOfFriend[unapprovedFriendsCount];
        for(int i = 0; i < unapprovedFriendsCount; i++) {
            unapprovedFriendsList[i] = unapprovedFriends.get(i);
        }

        int unreadMessageCount = unreadMessages.size();
        for(int i = 0; i < unreadMessageCount; i++) {
            messages[i] = unreadMessages.get(i);
            // todo: do some type of logging here to ensure that all unread messages are being created, this is very important.
        }

        this.updater.updateData((InfoOfMessage[]) messages, friendsList, unapprovedFriendsList, userKey);
        super.endDocument();
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {

        if(localName.equals("friend")) {
            InfoOfFriend friend = new InfoOfFriend();
            friend.userName = attributes.getValue(InfoOfFriend.USERNAME);
            String status = attributes.getValue(InfoOfFriend.STATUS);
            friend.port = attributes.getValue(InfoOfFriend.Port);

            if(status != null && status.equals("online")) {
                friend.status = InfoStatus.ONLINE;
                onlineFriends.add(friend);
            } else if(status.equals("unapproved")) {
                friend.status = InfoStatus.UNAPPROVED;
                unapprovedFriends.add(friend);
            } else {
                friend.status = InfoStatus.OFFLINE;
                friends.add(friend);
            }
        } else if(localName.equals("user")) {
            this.userKey = attributes.getValue(InfoOfFriend.UserKey);
        }

        super.startElement(uri, localName, name, attributes);

    }

    @Override
    public void startDocument() throws SAXException {
        this.friends.clear();
        this.onlineFriends.clear();
        this.unreadMessages.clear();
        super.startDocument();
    }
}
