package simplechat.thenotoriousrog.simplechat;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import simplechat.thenotoriousrog.simplechat.thenotoriousrog.simplechat.interfaces.Manager;

public class Login extends Activity {

    public static final int Connected_to_service = 0;
    public static final int FILL_BOTH_USERNAME_PASSWORD = 1;
    public static final String Authentication_Failed = "failed";
    public static final String Friend_List = "friend_list";
    public static final int Make_sure_username_password = 2;
    public static final int connected_to_network = 3;

    public EditText username;
    public EditText password;

    public Manager serviceProvider;

    public static final int sign_up = Menu.FIRST;
    public static final int exit = Menu.FIRST + 1;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            serviceProvider = ((MessagingService.IMBinder)service).getService();

            if(serviceProvider.isUserAuthenticated() == true) {
                Intent i = new Intent(Login.this, ListOfFriends.class);
                startActivity(i);
                Login.this.finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceProvider = null;
            Toast.makeText(Login.this, "Local service has stopped", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startService(new Intent(this, MessagingService.class));

        setContentView(R.layout.login);
        setTitle("Login");

        ImageButton loginButton =  findViewById(R.id.loginButton);

    }


}
